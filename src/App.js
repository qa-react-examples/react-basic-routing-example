import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './App.css';
import Home from './Home';
import About from './About';

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-Header">
          <Link to="/">Home</Link>
          <Link to="/about">About</Link>
        </header>
        <Route path="/" exact component={Home} />
        <Route path="/about" exact render={(props) => <About {...props}/>} />
      </div>
    </Router>
  );
}

export default App;
